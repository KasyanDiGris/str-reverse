#include <iostream>

void reverse_str(std::string& str) {
    for (int i = 0; i < str.length() / 2; i++) {
    	std::swap(str[i], str[str.length()-i-1]);
    } 
}

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cout << "Pass argument" << std::endl;
        return 1;
    }
    std::string str{argv[1]};
    reverse_str(str);
    std::cout << str << std::endl;
}
